# RESTful API

## Description
The idea of this API REST is provide the minimum necessary data to feed the front-end [chatsapp](https://gitlab.com/iraklimt/chatsapp). As the app behaviour is limited the API REST provides only two methods, GET and PUT. the GET method delivers both the users list and a unique user based on id, while the PUT method updates the users list.


## Architecture
The API REST is made by Laravel framework (v7.1) and uses an inbuilt class `FakeDatabase` to provide a simple array of users. As mentioned in the requirements and for simplicity, no databases are used for this API, but thanks to the `DatabaseAdapter class` is very easy to switch the data provider for any integrated DDBB in Laravel as mysql, sqlite, etc.

In order to avoid CORS policy issues, the installed `fruitcake/laravel-cors` package acts as a middleware providing the necessary http header configurations.

A simple inbuilt Validation method is triggered every time the user sends a PUT request.

## Data flow
The basic data flow follows the next pattern:
- When app is opened in the browser, the users are requested asynchronously, regardless the page navigation. 
- Once the data is delivered, the data management pass to the Vuex Store, by this, the app gains speed and performance. 
- When the target user updates any data, the data changes instantly in the view and an asynchronous request is sent to the REST.
- If the request fails, the previous state is displayed as fallback.

## Session management
Despite the project suggestion, in order to avoid incorrect practices, on server side no session is used to manage the data, but as implemented in the front end part, the user state remains in the browser session storage.

## Project setup
For simplicity and showcase the best way to test the app is just running it in development by the default Laravel artisan command. The default host used for local development is `http://localhost:8080/`
- `composer install`
- `php artisan serve`

