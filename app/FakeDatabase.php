<?php

namespace App;

class FakeDatabase {

    public $users =  [
        [
            "id" => 1,
            "name" => "Andrea",
            "age" => 32,
            "city" => "Barcelona",
            "friend" => true,
            "favorite" => true,
            "online" => true,
            "description" =>
                "I love working at TeamCMP for so many different reasons. We use cutting-edge technology, have industry professionals as our colleagues, and enjoy lots of perks and benefits that are unheard of in",
            "messages" => [],
        ],
        [
            "id" => 2,
            "name" => "Jessica",
            "age" => 28,
            "city" => "London",
            "friend" => false,
            "favorite" => false,
            "online" => false,
            "description" =>
                "I initially joined TeamCMP at the recommendation that it’s a great place to work from one of their competitors. If that doesn’t say something about the company, I don’t know what would. Since my...",
            "messages" => [],
        ],
        [
            "id" => 3,
            "name" => "Ricard",
            "age" => 34,
            "city" => "Bilbao",
            "friend" => false,
            "favorite" => false,
            "online" => false,
            "description" =>
                "Working at TeamCMP is still nowadays one of the best experiences I have in my life. I love the fact that there's never a boring day at the office, there's always an exciting problem to solve. I also",
            "messages" => [],
        ],
        [
            "id" => 4,
            "name" => "Pablo",
            "age" => 38,
            "city" => "Paris",
            "friend" => true,
            "favorite" => false,
            "online" => false,
            "description" =>
                "Since the first day, I felt at home in TeamCMP. I’ll never regret leaving my hometown and moving to Barcelona to work in such a great company. I love the work environment, the camaraderie that...",
            "messages" => [],
        ],
        [
            "id" => 5,
            "name" => "Laura",
            "age" => 31,
            "city" => "Paris",
            "friend" => false,
            "favorite" => true,
            "online" => true,
            "description" =>
                "I joined the Billing Team in 2016 and since that moment I have really enjoyed my experience with TeamCMP. Every day I see how dedicated and committed the Management is and how much",
            "messages" => [],
        ],
        [
            "id" => 6,
            "name" => "Nate",
            "age" => 36,
            "city" => "New York",
            "friend" => false,
            "favorite" => true,
            "online" => true,
            "description" =>
                "If I could go back in time and tell 20 years old me that in 5 years I'd be working at a place like TeamCMP, I don't think I would have believed it. The perks, the location, the people, the culture — every day, somethin...",
            "messages" => [],
        ],
        [
            "id" => 7,
            "name" => "Raluca",
            "age" => 28,
            "city" => "Milan",
            "friend" => false,
            "favorite" => false,
            "online" => true,
            "description" =>
                "When you start a job at TeamCMP you think, “oh well, just another company like many others,” but after getting there and getting to know people you realize that it’s more than that. Over time your so ...",
            "messages" => [],
        ],
        [
            "id" => 8,
            "name" => "Dani",
            "age" => 33,
            "city" => "London",
            "friend" => false,
            "favorite" => false,
            "online" => true,
            "description" =>
                "Working at TeamCMP is a really good combination of fun and hard work. When I first came for an interview I felt the atmosphere instantly. Here you are surrounded by very good professionals in each are...",
            "messages" => [],
        ],
        [
            "id" => 9,
            "name" => "Lenca",
            "age" => 26,
            "city" => "Bordeaux",
            "friend" => true,
            "favorite" => false,
            "online" => true,
            "description" =>
                "Working as a freelancer before, I thought I would never work for a company again. Until I discovered the challenging environment of TeamCMP. Here I am enabled to grow faster and further than I’d ",
            "messages" => [],
        ],
        [
            "id" => 10,
            "name" => "Miguel",
            "age" => 27,
            "city" => "Praga",
            "friend" => false,
            "favorite" => true,
            "online" => true,
            "description" =>
                "Working at TeamCMP is both an incredible work experience and personal one. We are a multicultural team working together, taking advantage of high level resources and trending technologies. All the...",
            "messages" => [],
        ],
    ];


    public function getUsers()
    {
        return $this->users;
    }

    public function setUsers($users)
    {
        $this->users = $users;
        return $this->users;
    }
}




