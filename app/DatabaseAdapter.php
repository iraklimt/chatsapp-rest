<?php

namespace App;

use App\FakeDatabase as DB;

class DatabaseAdapter extends DB {

    public $fake_users;

    public function __construct()
    {
        $this->fake_users = new DB();
    }

    public function all()
    {
        $users = collect($this->fake_users->getUsers());
        return $users;
    }

    public function find($id)
    {
        $users = collect($this->fake_users->getUsers());
        return $users->firstWhere('id', $id);
    }

    public function update($id, $item)
    {
        $users = $this->fake_users->getUsers();
        $updated_users = [];
        for($i=0; $i<count($users); $i++){
            if($users[$i]['id'] != $id){
                $updated_users[] = $users[$i];
            }
        }
        $updated_users[] = $item;
       return  $this->fake_users->setUsers($updated_users);
    }

}




