<?php

namespace App;

use App\DatabaseAdapter as Adapter;

class User extends Adapter
{
    public function getAll()
    {
        return $this->all();
    }

    public function getOne($id)
    {
        return $this->find($id);
    }

    public function updateOne($id, $item)
    {
        return $this->update($id, $item);
    }
}
